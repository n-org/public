#!/bin/bash

# export DOCKER_CLI_EXPERIMENTAL=enabled
# docker buildx create --name arm --platform linux/arm/v7
# docker buildx use arm
# docker buildx inspect --bootstrap
# docker buildx build --platform  linux/arm/v7 -t adamparco/demo:latest .
git clone https://github.com/dexidp/dex /tmp/dex
cd /tmp/dex
docker build -t registry.gitlab.com/n-org/public:dex-arm .
docker push registry.gitlab.com/n-org/public:dex-arm

