#!/bin/bash

git clone https://github.com/nirmata/kube-static-egress-ip
cd kube-static-egress-ip
docker build -t static-egress-ip-controller -f Dockerfile.controller .
docker build -t static-egress-ip-manager -f Dockerfile.manager .
docker tag static-egress-ip-manager registry.gitlab.com/n-org/public:static-egress-ip-manager-arm
docker tag static-egress-ip-controller registry.gitlab.com/n-org/public:static-egress-ip-controller-arm
docker push registry.gitlab.com/n-org/public:static-egress-ip-manager-arm
docker push registry.gitlab.com/n-org/public:static-egress-ip-controller-arm


