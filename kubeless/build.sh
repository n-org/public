#!/bin/bash

docker build --target bin     -t kubeless-bin .
docker build --target http    -t registry.gitlab.com/n-org/public:kubeless-http-trigger-arm .
docker build --target cronjob -t registry.gitlab.com/n-org/public:kubeless-cronjob-trigger-arm .
docker build --target function  -t registry.gitlab.com/n-org/public:kubeless-function-controller-arm .
docker build --target imbuilder -t registry.gitlab.com/n-org/public:kubeless-image-builder-arm .

docker push registry.gitlab.com/n-org/public:kubeless-http-trigger-arm 
docker push registry.gitlab.com/n-org/public:kubeless-cronjob-trigger-arm 
docker push registry.gitlab.com/n-org/public:kubeless-function-controller-arm 
docker push registry.gitlab.com/n-org/public:kubeless-image-builder-arm 
