#!/bin/bash
# docker build -t registry.gitlab.com/n-org/public:agensgraph-amd64 -f Dockerfile .
# docker push registry.gitlab.com/n-org/public:agensgraph-amd64

docker build -t registry.gitlab.com/n-org/public:agensgraph-light-amd64 -f Dockerfile-light .
docker push registry.gitlab.com/n-org/public:agensgraph-light-amd64

docker build -t registry.gitlab.com/n-org/public:agensgraph-base-amd64 -f Dockerfile-base .
docker push registry.gitlab.com/n-org/public:agensgraph-base-amd64
