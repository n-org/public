#!/bin/bash

for LDAP_GROUP in reader updater writer manager admin;
do
    for LDAP_USER in $( ldapsearch \
                            -b $LDAP_SYNC_BINDDN \
                            -D $LDAP_SYNC_DN \
                            -w $LDAP_SYNC_PASSWORD \
                            -H $LDAP_SYNC_URI -LLL "(&(objectClass=groupOfNames)(cn=$LDAP_GROUP))" member \
                        | grep -oP "(?<=member: uid\=)(.*)(?=,ou\=people,dc\=n,dc\=org)" );
    do
        # Create User if doesn't exist
        if ! [ "$(./bin/agens -tAc "SELECT 1 FROM pg_roles WHERE rolname = '$LDAP_USER'")" = '1' ];
        then
            echo "CREATE ROLE \"$LDAP_USER\" LOGIN;" | /agensgraph/bin/agens $AG_DBNAME
        fi
        
        # Grant permittions to group
        echo "GRANT $LDAP_GROUP TO \"$LDAP_USER\";" | /agensgraph/bin/agens $AG_DBNAME

    done
done
