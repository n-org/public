#!/bin/bash


# Start Server
if [ "$1" = "agensgraph" ];then
    

    # Initialize database
    # -------------------
    # 
    if [ ! -d $AG_DATA ];then
        su agens -c "/agensgraph/bin/initdb -D $AG_DATA"
        su agens -c "/agensgraph/bin/ag_ctl -D $AG_DATA start"
        su agens -c "/agensgraph/bin/createdb  $AG_DBNAME"
        cat <<EOF | su agens -c "/agensgraph/bin/agens"
        --- Create extentions 
        CREATE EXTENSION pgcrypto;

        --- Disable default ability to create objects
        REVOKE CREATE ON SCHEMA public FROM PUBLIC;

        --- Revoke default abilities on our DB 
        REVOKE ALL ON DATABASE $AG_DBNAME FROM PUBLIC;
        
        -- Reader can read data from existing db objects (tables,views,...)
        CREATE ROLE reader  NOLOGIN INHERIT;
        GRANT CONNECT ON DATABASE                    $AG_DBNAME                      TO reader;
        GRANT USAGE   ON SCHEMA                      public                          TO reader;
        GRANT SELECT  ON ALL TABLES    IN SCHEMA     public                          TO reader;
        GRANT SELECT  ON ALL SEQUENCES IN SCHEMA     public                          TO reader;
        ALTER DEFAULT PRIVILEGES IN SCHEMA           public GRANT SELECT ON TABLES   TO reader;

        -- Updater can read/update data on existing db objects (tables,views,...)
        CREATE ROLE updater NOLOGIN INHERIT;
        GRANT CONNECT ON DATABASE                    $AG_DBNAME                      TO updater;
        GRANT USAGE   ON SCHEMA                      public                          TO updater;
        GRANT SELECT, UPDATE ON ALL TABLES IN SCHEMA public                          TO updater;
        ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT, UPDATE ON TABLES     TO updater;
        GRANT USAGE,SELECT  ON ALL SEQUENCES IN SCHEMA     public                          TO updater;
        ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT USAGE ON SEQUENCES           TO updater;

        -- Writer can read/update/write/delete data on existing db objects (tables,views,...)
        CREATE ROLE writer  NOLOGIN INHERIT;
        GRANT CONNECT ON DATABASE                    $AG_DBNAME                      TO writer;
        GRANT USAGE   ON SCHEMA                      public                          TO writer;
        GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public          TO writer;
        ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO writer;
        GRANT USAGE,SELECT,UPDATE ON ALL SEQUENCES IN SCHEMA     public              TO writer;
        ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT USAGE ON SEQUENCES           TO writer;
  

        -- Admin can CREATE db objects (tables,views,...)
        CREATE ROLE admin NOLOGIN INHERIT;
        GRANT ALL PRIVILEGES ON DATABASE agens TO admin;
        GRANT CONNECT ON DATABASE                           $AG_DBNAME                      TO admin;
        GRANT ALL  ON SCHEMA                                public                          TO admin;
        GRANT ALL  ON ALL TABLES IN SCHEMA                  public                          TO admin;
        ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL ON TABLES                       TO admin;
        GRANT ALL  ON ALL SEQUENCES IN SCHEMA               public                          TO admin;
        ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL ON SEQUENCES                    TO admin;

EOF
        # Create Default Graph
        cat <<EOF | su agens -c "/agensgraph/bin/agens"
        -- Create graph
        CREATE GRAPH IF NOT EXISTS $AG_GRAPH_NAME;

        -- Set default graph path for groups
        ALTER ROLE reader  IN DATABASE $AG_DBNAME SET graph_path TO $AG_GRAPH_NAME; 
        ALTER ROLE updater IN DATABASE $AG_DBNAME SET graph_path TO $AG_GRAPH_NAME; 
        ALTER ROLE writer  IN DATABASE $AG_DBNAME SET graph_path TO $AG_GRAPH_NAME; 
        ALTER ROLE admin   IN DATABASE $AG_DBNAME SET graph_path TO $AG_GRAPH_NAME; 

        -- ??? 
        -- __TODO__ : 
        --      Fix permittions for graph
        --
        GRANT ALL  ON SCHEMA                $AG_GRAPH_NAME TO admin;
        GRANT ALL  ON ALL TABLES IN SCHEMA  $AG_GRAPH_NAME TO admin;
        GRANT ALL  ON ALL SEQUENCES IN SCHEMA  $AG_GRAPH_NAME TO admin;
        
        -- Set default graph path for database
        ALTER DATABASE $AG_DBNAME SET graph_path TO $AG_GRAPH_NAME;
EOF

        su agens -c "/agensgraph/bin/ag_ctl -D $AG_DATA stop"

        # Listen on all interfaces
        if ! grep -q '^listen_addresses' $AG_DATA/postgresql.conf;then
            echo "listen_addresses = '*'" | su agens -c "tee -a $AG_DATA/postgresql.conf"
        fi

    fi

    # Configure ldap authentication 
    # -----------------------------
    #
    if $AG_LDAP_AUTH;then
        if ! grep -q 'ldapserver' $AG_DATA/pg_hba.conf;then
            cat <<EOF |  su agens -c "tee -a $AG_DATA/pg_hba.conf"
host  all  all  0.0.0.0/0 ldap ldapserver=$AG_LDAP_SERVER ldapport=$AG_LDAP_PORT ldapprefix="$AG_LDAP_PREFIX" ldapsuffix="$AG_LDAP_SUFFIX"
EOF
        fi
    fi

    su agens -c "/agensgraph/bin/postgres -D $AG_DATA $AG_OPTS"

elif [ "$1" = "ldapsync" ];then
    
    # __TODO__:: SETUP PROPER CronJOB
    
    # https://stackoverflow.com/questions/37458287/how-to-run-a-cron-job-inside-a-docker-container
    # echo "* * * * * su agens -c /agensgraph/ldap_sync.sh" | crontab -u agens -
    # "cron", "-f"
    su agens -c "/agensgraph/ldap_sync.sh"

else
    sh -c "$@"
fi
