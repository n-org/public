#!/bin/bash


# Start Server
if [ "$1" = "agensgraph" ];then
    

    # Initialize database
    # -------------------
    # 
    if [ ! -d $AG_DATA ];then

        su agens -c "/agensgraph/bin/initdb -D $AG_DATA --encoding='UTF-8'"
        su agens -c "/agensgraph/bin/ag_ctl -D $AG_DATA start"
        su agens -c "/agensgraph/bin/createdb  --encoding='UTF-8' $AG_DBNAME"
        cat <<EOF | /agensgraph/bin/agens -U agens
        --- Create extentions 
        CREATE EXTENSION pgcrypto;
        CREATE EXTENSION plpython3u;

        --- Disable default ability to create objects
        REVOKE CREATE ON SCHEMA public FROM PUBLIC;

        -- Create AG_USER
        CREATE ROLE $AG_USER WITH PASSWORD '$AG_PASSWORD' LOGIN;

        GRANT ALL PRIVILEGES ON DATABASE $AG_DBNAME TO $AG_USER;
        GRANT CONNECT        ON DATABASE $AG_DBNAME TO $AG_USER;
        
        GRANT ALL  ON SCHEMA public                  TO $AG_USER;
        GRANT ALL  ON ALL TABLES    IN SCHEMA public TO $AG_USER;
        GRANT ALL  ON ALL SEQUENCES IN SCHEMA public TO $AG_USER;
        
        ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL ON TABLES    TO $AG_USER;
        ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL ON SEQUENCES TO $AG_USER;
EOF
        cat <<EOF | /agensgraph/bin/agens -U $AG_USER -d $AG_DBNAME
        -- Create graph
        CREATE GRAPH IF NOT EXISTS $AG_GRAPH_NAME;

        -- Set default graph path for User
        ALTER ROLE $AG_USER   IN DATABASE $AG_DBNAME SET graph_path TO $AG_GRAPH_NAME;

        -- Set permitions
        -- GRANT ALL  ON SCHEMA                $AG_GRAPH_NAME TO $AG_USER;
EOF
        su agens -c "/agensgraph/bin/ag_ctl -D $AG_DATA stop"

        # Listen on all interfaces
        if ! grep -q '^listen_addresses' $AG_DATA/postgresql.conf;then
            echo "listen_addresses = '*'" \
                | su agens -c "tee -a $AG_DATA/postgresql.conf"
        fi

        # Allow Authentication
        if ! grep -q 'host  all  all  0.0.0.0/0 md5' $AG_DATA/pg_hba.conf;then
            echo "host  all  all  0.0.0.0/0 md5" \
                | su agens -c "tee -a $AG_DATA/pg_hba.conf"
        fi



    fi

    su agens -c "/agensgraph/bin/postgres -D $AG_DATA $AG_OPTS"

else
    sh -c "$@"
fi
