#!/bin/bash


# Start Server
if [ "$1" = "agensgraph" ];then
    

    # Initialize database
    # -------------------
    # 
    if [ ! -d $AG_DATA ];then

        su agens -c "/agensgraph/bin/initdb -D $AG_DATA --encoding='UTF-8'"
        su agens -c "/agensgraph/bin/ag_ctl -D $AG_DATA start"
        su agens -c "/agensgraph/bin/createdb  --encoding='UTF-8' $AG_DBNAME"
        
        if [ -d /docker-entrypoint-initdb.d/ ];then
            # User defined init scripts similar to https://hub.docker.com/_/postgres/ 
            find /docker-entrypoint-initdb.d/ -type f -name *.sh  -exec su agens -c "bash {}" \;
            find /docker-entrypoint-initdb.d/ -type f -name *.sql -exec su agens -c "/agensgraph/bin/agens -f {}" \;
        fi

        su agens -c "/agensgraph/bin/ag_ctl -D $AG_DATA stop"
        
        # Listen on all interfaces
        if ! grep -q '^listen_addresses' $AG_DATA/postgresql.conf;then
            echo "listen_addresses = '*'" \
                | su agens -c "tee -a $AG_DATA/postgresql.conf"
        fi

        # Allow Authentication
        if ! grep -q 'host  all  all  0.0.0.0/0 md5' $AG_DATA/pg_hba.conf;then
            echo "host  all  all  0.0.0.0/0 md5" \
                | su agens -c "tee -a $AG_DATA/pg_hba.conf"
        fi

    fi

    su agens -c "/agensgraph/bin/postgres -D $AG_DATA $AG_OPTS"

else
    sh -c "$@"
fi
