#!/bin/bash

git clone https://github.com/codimd/container
cd container/
docker build -f alpine/Dockerfile -t registry.gitlab.com/n-org/public:codimd-arm .
docker push registry.gitlab.com/n-org/public:codimd-arm
