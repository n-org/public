Debugging showed that the error was that backend could't load the query sent from frontend.
After fixing that error on src/server/index.js using bodyParser the DB pool connection stuck for some reason. Then installed agensgraph-nodejs as described under https://github.com/bitnine-oss/agensgraph-nodejs and things seems to work.


```bash
git clone https://github.com/mrjj/agens-viewer
cd /agens-viewer
git checkout 0.1.4
npm install pegjs
npm install pg
npm install git+https://github.com/bitnine-oss/agensgraph-nodejs.git
npm install body-parser
```


**src/server/index.js**
```js
...
const bodyParser = require('body-parser');
...
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.text({"type": "text/*"}));
```
